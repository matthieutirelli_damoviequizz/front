import axios from 'axios';

export default class GameFetcher {
    constructor({ api_url }) {
        this.instance = axios.create({
            baseURL: api_url,
            timeout: 10000,
        });
    }

    getGame() {
        return this.instance.get('/game/start');
    }

    getChallenge(game_uuid) {
        return this.instance.get(`/game/${game_uuid}/challenge`);
    }

    answerChallenge(game_uuid, challenge_uuid, answer) {
        return this.instance.post(
            `/game/${game_uuid}/challenge/${challenge_uuid}/answer`,
            { answer }
            );
    }
}