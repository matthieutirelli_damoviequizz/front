import React from "react";
import Button from "./Button";

const ChallengeContainer = ({ challenge, callbackAction }) => {

    if (!challenge) {
        return (
            <div className="challenge-center"><h1><i className={"fa fa-spin fa-spinner"} /></h1></div>
        );
    }

    return (
        <div className="challenge-container">
            <div className="challenge-data">
                <img src={challenge.picture} alt={`Movie ${challenge.title} poster`}/>
                { challenge.actor_picture ? <img className="actor-picture" src={challenge.actor_picture} alt={`Actor ${challenge.actor_picture} poster`} /> : null}
                <h1>{ challenge.actor }</h1>
                <h2>est dans le casting du film</h2>
                <h1>{ challenge.title }</h1>
            </div>
            <div className="challenge-actions">
                <Button
                    value="OUI"
                    type="action-true"
                    onClick={() => callbackAction(true)}
                />
                <Button
                    value="NON"
                    type="action-false"
                    onClick={() => callbackAction(false)}
                />
            </div>
        </div>
    );
};

export default ChallengeContainer;