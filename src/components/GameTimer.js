import React from 'react';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';

export default class GameTimer extends React.Component {
    render() {

        const { game } = this.props;

        const renderTime = value => {
            if (value === 0) {
                return <div className="timer">Terminé</div>;
            }

            return (
                <div className="timer">
                    <div className="text">Il reste</div>
                    <div className="value">{value}</div>
                    <div className="text">secondes</div>
                </div>
            );
        };

        return (
            <>
                <div className="game-countdown-circle-timer">
                    <CountdownCircleTimer
                        isPlaying
                        durationSeconds={game.duration}
                        renderTime={renderTime}
                        colors={[
                            ['#004777', .33],
                            ['#F7B801', .33],
                            ['#A30000']
                        ]}
                    />
                </div>
            </>
        );
    }
}