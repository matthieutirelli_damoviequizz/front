import React from "react";

const Button = ({value, type, onClick}) => {
    return (
        <button className={"btn " + type} onClick={onClick}>{ value }</button>
    )
};

export default Button;