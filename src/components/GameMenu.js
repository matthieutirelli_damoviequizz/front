import React from 'react';
import StatCard from "./StatCard";
import GameTimer from "./GameTimer";

const GameMenu = ({ game, history }) => {

    if (!game) {
        return null;
    }

    return (
        <div className="menu">
            <GameTimer
                game={game}
            />
            <div className="score-card">
                <StatCard
                    title="POINTS"
                    value={<span className="status-success">{ game.points }</span>}
                />
            </div>
            <div className={"history-games"}>
                { history.map(challenge =>
                    <StatCard
                        key={challenge.uuid}
                        title={ challenge.title }
                    />
                )}
            </div>
        </div>
    );
};

export default GameMenu;