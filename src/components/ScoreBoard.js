import React from 'react';
import Button from "./Button";

const ScoreBoard = ({ game, restartGame }) => {
  return (
      <div className="centered">
          <h1>La partie est terminée, vous avez accumulé { game.points } points !</h1>
          <div>
              <Button
                  value="Nouvelle partie"
                  type="action-true"
                  onClick={ () => restartGame() }
              />
          </div>
      </div>
  )
};

export default ScoreBoard;