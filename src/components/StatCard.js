import React from 'react';

const StatCard = ({ title, value }) => {
    return (
        <div className="stat-card">
            <div>
                { title }
            </div>
            { value }
        </div>
    );
};

export default StatCard;