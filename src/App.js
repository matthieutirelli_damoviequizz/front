import React from 'react';
import './App.css';
import GameMenu from "./components/GameMenu";
import GameFetcher from "./fetcher/GameFetcher";
import ChallengeContainer from "./components/ChallengeContainer";
import Button from "./components/Button";
import ScoreBoard from "./components/ScoreBoard";

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.gamefetcher = new GameFetcher({
            api_url: 'https://damoviequizz.cokeys.fr/'
        });
        this.state = {
            game: null,
            tick: null,
            history: [],
            challenge: null,
            finish: false
        }
    }

    getGame = () => {
        this.gamefetcher.getGame().then(response =>
            this.setState({
                game: response.data,
                finish: false,
                challenge: null,
                history: []
            }, () => {
                setTimeout(this.finishGame, this.state.game.duration * 1000);
                this.getChallenge()
            })
        );
    };

    finishGame = () => {
        this.setState({
            challenge: null,
            finish: true
        });
    };

    setChallengeResult = ({ message, points}) => {

        const { game } = this.state;

        switch (message) {
            case "MSG_GAME_SUCCESS":
                game.points = points;
                this.setState({
                    challenge: null,
                    game
                }, () => {
                    this.getChallenge();
                    console.log(game);
                });
                break;
            case "MSG_GAME_FAILURE":
                return this.finishGame();
            case "MSG_GAME_TIMEOUT":
                return this.finishGame();

            default:
                console.log("Error");
        }
    };

    getChallenge = () => {
        const { game, history } = this.state;
        this.gamefetcher.getChallenge(game.uuid).then(response => {
            history.push(response.data);
            this.setState({
                challenge: response.data,
                history
            });
        }
        );
    };

    clearGame = () => {
        this.setState({
            game: null,
        })
    };


    answerChallenge = (answer) => {
        const { game, challenge } = this.state;
        this.gamefetcher.answerChallenge(game.uuid, challenge.uuid, answer).then(response => {
           this.setChallengeResult(response.data);
        });
    };

    render() {
        const { game, challenge, history, finish } = this.state;

        if (!game) {
            return (
                <div className="challenge-center">
                    <Button
                        value="Nouvelle partie"
                        type="action-true"
                        onClick={ this.getGame }
                    />
                </div>
            );
        }

        if (finish) {
            return <ScoreBoard
                game={game}
                restartGame={ this.getGame }
            />;
        }

        return (
            <div className="game-screen">
                <div className="game">
                    <ChallengeContainer
                        challenge={challenge}
                        callbackAction={ this.answerChallenge }
                    />
                </div>
                <GameMenu
                    game={game}
                    history={history}
                />
            </div>
        )
    }
};